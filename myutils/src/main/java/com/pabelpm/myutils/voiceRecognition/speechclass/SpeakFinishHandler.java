package com.pabelpm.myutils.voiceRecognition.speechclass;

public interface SpeakFinishHandler {
    void onFinish();
}
