package com.pabelpm.myutils.voiceRecognition.speechclass;

public interface SpeechRecognitionFinishHandler {
    void onPlayVibration();
    void onPauseVibration();
}
