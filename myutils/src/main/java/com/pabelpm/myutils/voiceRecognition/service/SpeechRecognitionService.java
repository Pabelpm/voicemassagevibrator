package com.pabelpm.myutils.voiceRecognition.service;

/**
 * Created by Pabel on 26/03/2018.
 **/

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.widget.Toast;

import com.pabelpm.myutils.Constants;

import java.util.ArrayList;


public class SpeechRecognitionService extends Service {
    static final String TAG = Constants.Const.getAPP_FILTER() + SpeechRecognitionService.class.getSimpleName();
    // Binder given to clients
    private final IBinder binder = new LocalBinder();
    private  ServiceCallbacks serviceCallbacks;
    // Class used for the client Binder.
    public class LocalBinder extends Binder {
        public SpeechRecognitionService getService() {
            // Return this instance of MyService so clients can call public methods
            return SpeechRecognitionService.this;
        }
    }
    public void setCallbacks(ServiceCallbacks callbacks) {
        serviceCallbacks = callbacks;
    }
    public void removeCallbacks(){
        serviceCallbacks = null;
    }

    private SpeechRecognizer mSpeechRecognizer;
    private Intent mSpeechRecognizerIntent;

    interface MyCallback {
        void reiniciarCallback();

        void errorCallback(int error);

        void resultCallback(ArrayList<String> stringArrayList);
    }

    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
     * Service
     *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    //region Service
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreateService");
        loadSpeechRecognizer();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroyService");
        finishSpeechRecognition();
    }

    //endregion

    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
    * Auxiliar
    *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    //region Auxiliar
    private void loadSpeechRecognizer() {
        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
        mSpeechRecognizer.setRecognitionListener(new SpeechRecognitionListener(new SpeechRecognitionService.MyCallback() {
            @Override
            public void reiniciarCallback() {
                Log.d(TAG, "reiniciarCallback");
                mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
            }

            @Override
            public void errorCallback(int error) {
                String sError = "";
                switch (error) {
                    case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                        sError = "ERROR_NETWORK_TIMEOUT";
                        break;
                    case SpeechRecognizer.ERROR_NETWORK:
                        sError = "ERROR_NETWORK";
                        break;
                    case SpeechRecognizer.ERROR_AUDIO:
                        sError = "ERROR_AUDIO";
                        break;
                    case SpeechRecognizer.ERROR_SERVER:
                        sError = "ERROR_SERVER";
                        break;
                    case SpeechRecognizer.ERROR_CLIENT:
                        sError = "ERROR_CLIENT";
                        break;
                    case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                        reiniciarSpeech();
                        sError = "ERROR_SPEECH_TIMEOUT";
                        break;
                    case SpeechRecognizer.ERROR_NO_MATCH:
                        reiniciarSpeech();
                        sError = "ERROR_NO_MATCH";
                        break;
                    case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                        sError = "ERROR_RECOGNIZER_BUSY";
                        break;
                    case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                        sError = "ERROR_INSUFFICIENT_PERMISSIONS";
                        Toast.makeText(SpeechRecognitionService.this, "Por favor active el permiso de microfono a mano", Toast.LENGTH_LONG).show();
                        break;
                }
                Log.w(TAG, "error = " + sError + "integer: " + error);
            }

            @Override
            public void resultCallback(ArrayList<String> arrayResult) {
                if (arrayResult.contains("play")){
                    serviceCallbacks.onPlayVibration();
                }else if (arrayResult.contains("stop")){
                    serviceCallbacks.onStopVibration();
                }else if (arrayResult.contains("despacio")){
                    serviceCallbacks.onSlowVibration();
                }else if (arrayResult.contains("normal")){
                    serviceCallbacks.onNormalVibration();
                }else if (arrayResult.contains("rápido")){
                    serviceCallbacks.onFastVibration();
                }else if (arrayResult.contains("más")){
                    serviceCallbacks.onMoreVibration();
                }else if (arrayResult.contains("menos")){
                    serviceCallbacks.onLessVibration();
                }
                reiniciarSpeech();
            }
        }));
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, this.getPackageName());

        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getApplicationContext().getPackageName());

        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 10);

        mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
    }

    public void reiniciarSpeech() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 0,5s = 500ms
                mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
            }
        }, 100);
    }


    public void stopSpeechRecognition() {
        Log.i(TAG, "stop speechRecognition");
        if (mSpeechRecognizer == null) return;
        try {
            mSpeechRecognizer.stopListening();
        } catch (IllegalArgumentException e) {
            mSpeechRecognizer = null;
        }
    }

    public void finishSpeechRecognition() {
        Log.i(TAG, "finish speechRecognition");
        if (mSpeechRecognizer == null) return;
        try {
            mSpeechRecognizer.cancel();
            mSpeechRecognizer.stopListening();
            mSpeechRecognizer.destroy();
            mSpeechRecognizer = null;
        } catch (IllegalArgumentException e) {
            mSpeechRecognizer = null;
        }
    }
    //endregion
}
