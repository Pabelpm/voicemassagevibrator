package com.pabelpm.myutils.voiceRecognition;
import android.app.Activity;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import com.pabelpm.myutils.Constants;
import com.pabelpm.myutils.voiceRecognition.speechclass.SpeakFinishHandler;

import java.util.HashMap;
import java.util.Locale;


/**
 * Created by Pabel on 26/03/2018.
 **/

public class TextToSpeak implements TextToSpeech.OnInitListener {
    private static final String TAG = Constants.Const.getAPP_FILTER()+TextToSpeak.class.getSimpleName();

    private TextToSpeech textToSpeech;
    private static TextToSpeak textToSpeak;


    public static TextToSpeak getTextToSpeak(Activity activity) {
        if (textToSpeak == null) {
            Log.i(TAG, "new TextToSpeak");
            textToSpeak = new TextToSpeak(activity);
        }
        return textToSpeak;
    }
    public TextToSpeak(Activity activity) {
        this.textToSpeech = new TextToSpeech(activity, this);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            int result = textToSpeech.setLanguage(Locale.getDefault());

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }
    }

    public TextToSpeech getTextToSpeech() {
        return textToSpeech;
    }

    public void setTextToSpeech(TextToSpeech textToSpeech) {
        this.textToSpeech = textToSpeech;
    }

    public static void speak(Activity activity, String text, final SpeakFinishHandler onTtsFinishHandler) {
        TextToSpeech tts = TextToSpeak.getTextToSpeak(activity).getTextToSpeech();
        Locale locale = new Locale("es","ES");
        tts.setLanguage(locale);
        tts.setPitch(1.15f);
        tts.setSpeechRate(1.3f);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                @Override
                public void onDone(String utteranceId) {
                    if (onTtsFinishHandler != null)
                        onTtsFinishHandler.onFinish();
                }

                @Override
                public void onError(String utteranceId) {
                }

                @Override
                public void onStart(String utteranceId) {
                }

            });

            if (text != null) {
                text = text.replace("+", "plus").replace("DNI", "d    ene   ii");
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                tts.speak(text, TextToSpeech.QUEUE_ADD, null, "utteranceId");
            } else {
                HashMap<String, String> hashTts = new HashMap<String, String>();
                hashTts.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "utteranceId");
                tts.speak(text, TextToSpeech.QUEUE_ADD, hashTts);
            }
        }

    }
}
