package com.pabelpm.myutils.voiceRecognition.speechclass;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;

import com.pabelpm.myutils.Constants;

import java.util.ArrayList;

/**
 * Created by Pabel on 26/03/2018.
 **/

public class SpeechRecognition implements RecognitionListener {
    static final String TAG = Constants.Const.getAPP_FILTER() + SpeechRecognition.class.getSimpleName();

    private static SpeechRecognition mSpeechRecognition = null;

    private Activity mActivity;
    private SpeechRecognizer mSpeechRecognizer;
    private Intent mSpeechRecognizerIntent;

    private static SpeechRecognitionFinishHandler mSpeechRecognitionFinishHandler;

    public static SpeechRecognition getSpeechRecognition() {
        Log.i(TAG, "Create Instance SpeechRecognition");

        if (mSpeechRecognition == null) {
            mSpeechRecognition = new SpeechRecognition();
        }
        return mSpeechRecognition;
    }

    public void init(Activity activity, SpeechRecognitionFinishHandler speechRecognitionFinishHandler) {
        mSpeechRecognitionFinishHandler = speechRecognitionFinishHandler;
        mActivity = activity;
    }

    public SpeechRecognizer getmSpeechRecognizer() {
        return mSpeechRecognizer;
    }

    public void setmSpeechRecognizer(SpeechRecognizer mSpeechRecognizer) {
        this.mSpeechRecognizer = mSpeechRecognizer;
    }

    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
    * Auxiliar
    *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    //region Auxiliar
    public void loadSpeechRecognizer() {
        Log.v(TAG, "loadSpeechRecognizer");
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, mActivity.getPackageName());

        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS, 3000000);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, 3000000);

        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(mActivity);
        mSpeechRecognizer.setRecognitionListener(this);
        mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
    }


    public void stopSpeechRecognition() {
        Log.i(TAG, "stop speechRecognition");
        if (mSpeechRecognizer == null) return;
        try {
            mSpeechRecognizer.stopListening();
        } catch (IllegalArgumentException e) {
            mSpeechRecognizer = null;
        }
    }

    public void finishSpeechRecognition() {
        Log.i(TAG, "finish speechRecognition");
        if (mSpeechRecognizer == null) return;
        try {
            mSpeechRecognizer.cancel();
            mSpeechRecognizer.stopListening();
            mSpeechRecognizer.destroy();
            mSpeechRecognizer = null;
        } catch (IllegalArgumentException e) {
            mSpeechRecognizer = null;
        }
    }

    /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*
    * Implements Methods
    *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
    //region Implements Methods
    @Override
    public void onBeginningOfSpeech() {
        // speech input will be processed, so there is no need for count down anymore
        Log.v(TAG, "onBeginingOfSpeech");
    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.v(TAG, "onBufferReceived");

    }

    @Override
    public void onEndOfSpeech() {
        Log.v(TAG, "onEndOfSpeech");
        mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
    }

    @Override
    public void onError(int error) {
        String sError = "";
        switch (error) {
            case SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                sError = "ERROR_NETWORK_TIMEOUT";
                break;
            case SpeechRecognizer.ERROR_NETWORK:
                sError = "ERROR_NETWORK";
                break;
            case SpeechRecognizer.ERROR_AUDIO:
                sError = "ERROR_AUDIO";
                break;
            case SpeechRecognizer.ERROR_SERVER:
                sError = "ERROR_SERVER";
                break;
            case SpeechRecognizer.ERROR_CLIENT:
                sError = "ERROR_CLIENT";
                break;
            case SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                sError = "ERROR_SPEECH_TIMEOUT";
                break;
            case SpeechRecognizer.ERROR_NO_MATCH:
                sError = "ERROR_NO_MATCH";
                break;
            case SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                sError = "ERROR_RECOGNIZER_BUSY";
                break;
            case SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                sError = "ERROR_INSUFFICIENT_PERMISSIONS";
                break;
        }
        Log.w(TAG, "error = " + sError + "integer: " + error);
    }

    @Override
    public void onEvent(int eventType, Bundle params) {
        Log.v(TAG, "onEvent ");

    }

    @Override
    public void onPartialResults(Bundle partialResults) {
        Log.v(TAG, "onPartialResults ");

    }

    @Override
    public void onReadyForSpeech(Bundle params) {
        Log.v(TAG, "onReadyForSpeech");
    }

    @Override
    public void onResults(Bundle results) {
        ArrayList<String> arrayResult = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        if (arrayResult != null) {
            Log.i(TAG, "onResults " + arrayResult.toString());
        }
        if(arrayResult.toString().contains("play")){
            mSpeechRecognitionFinishHandler.onPlayVibration();
        }
        mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
    }

    @Override
    public void onRmsChanged(float rmsdB) {
        //Log.v(TAG, "onRmsChanged " + rmsdB);
    }
    //endregion

}
