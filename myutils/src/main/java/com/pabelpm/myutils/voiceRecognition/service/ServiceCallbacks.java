package com.pabelpm.myutils.voiceRecognition.service;

/**
 * Created by Pabel on 20/04/2018.
 **/

public interface ServiceCallbacks {
    void onPlayVibration();
    void onStopVibration();
    void onSlowVibration();
    void onNormalVibration();
    void onFastVibration();
    void onMoreVibration();
    void onLessVibration();
}
