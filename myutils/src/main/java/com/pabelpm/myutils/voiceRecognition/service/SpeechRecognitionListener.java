package com.pabelpm.myutils.voiceRecognition.service;

/**
 * Created by Pabel on 26/03/2018.
 **/

import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.SpeechRecognizer;
import android.util.Log;

import com.pabelpm.myutils.Constants;

import java.util.ArrayList;

public class SpeechRecognitionListener implements RecognitionListener {
    private static final String TAG = Constants.Const.getAPP_FILTER() + SpeechRecognitionListener.class.getSimpleName();

    private SpeechRecognitionService.MyCallback myCallback;

    public SpeechRecognitionListener(SpeechRecognitionService.MyCallback callback) {
        myCallback = callback;
    }

    @Override
    public void onBeginningOfSpeech() {
        // speech input will be processed, so there is no need for count down anymore
        Log.d(TAG, "onBeginingOfSpeech");
    }

    @Override
    public void onBufferReceived(byte[] buffer) {
        Log.d(TAG, "onBufferReceived");
    }

    @Override
    public void onEndOfSpeech() {
        Log.d(TAG, "onEndOfSpeech");
    }

    @Override
    public void onError(int error) {
        myCallback.errorCallback(error);
    }

    @Override
    public void onEvent(int eventType, Bundle params) {
        Log.d(TAG, "onEvent ");

    }

    @Override
    public void onPartialResults(Bundle partialResults) {
        Log.d(TAG, "onPartialResults ");
    }

    @Override
    public void onReadyForSpeech(Bundle params) {
        Log.d(TAG, "onReadyForSpeech");
    }

    @Override
    public void onResults(Bundle results) {
        Log.d(TAG, "onResults ");
        ArrayList<String> arrayResult = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        if (arrayResult != null) {
            Log.d(TAG, "onResults " + arrayResult.toString());
        }
        myCallback.resultCallback(arrayResult);
    }

    @Override
    public void onRmsChanged(float rmsdB) {
        //Log.d(TAG, "onRmsChanged " + rmsdB); //$NON-NLS-1$
    }

}