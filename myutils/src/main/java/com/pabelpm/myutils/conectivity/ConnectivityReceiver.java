package com.pabelpm.myutils.conectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.pabelpm.myutils.Constants;

import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * Created by Pabel on 26/03/2018.
 **/

public class ConnectivityReceiver extends BroadcastReceiver {
    private final static String TAG = Constants.Const.getAPP_FILTER() + ConnectivityReceiver.class.getSimpleName();

    private ConnectivityReceiverListener mConnectivityReceiverListener;

    ConnectivityReceiver(ConnectivityReceiverListener listener) {
        mConnectivityReceiverListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String status;
        if (intent.getExtras() != null) {
            NetworkInfo ni = (NetworkInfo) intent.getExtras().get(ConnectivityManager.EXTRA_NETWORK_INFO);
            if (ni != null && ni.getState() == NetworkInfo.State.CONNECTED) {
                ConnectivityManager manager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
                boolean isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
                if (!isWifi) {
                    status = "Connected 3G";
                    Log.i(TAG, "Network " + ni.getTypeName() + " Connected 3G");
                } else {
                    status = "Connected Wifi";
                    Log.i(TAG, "Network " + ni.getTypeName() + " Connected Wifi");
                }
            } else if (ni != null && ni.getState() == NetworkInfo.State.DISCONNECTED) {
                Log.i(TAG, "Network " + ni.getTypeName() + " Disconnected");
                status = "Disconnected";
            } else if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
                Log.d(TAG, "There's no network connectivity");
                status = "No connectivity";
            } else {
                Log.d(TAG, "No info network");
                status = "No info network";
            }
            mConnectivityReceiverListener.onNetworkConnectionChanged(status);
        }

    }

    public interface ConnectivityReceiverListener {
        void onNetworkConnectionChanged(String statusConnected);
    }
}