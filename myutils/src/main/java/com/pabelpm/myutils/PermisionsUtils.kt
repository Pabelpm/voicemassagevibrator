package com.pabelpm.myutils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Resources
import android.support.v4.app.ActivityCompat
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

/**
 * Created by Pabel on 26/03/2018.
 **/
class PermisionsUtils {
    companion object Const {
        var KEYLOG = PermisionsUtils::class.java.simpleName

        //************************PERMISSIONS*******************************//

        /**
         * Setup the permissions.
         */
        fun requestCustomPermissions(activity: Activity) {
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO), 10)
            } else {
                Log.i(KEYLOG, "All permission was successfully granted.")
            }
        }

        /**
         * Request the download storage permission.
         */
        fun requestStoragePermission(activity: Activity) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
            } else {
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
            }
        }

        /**
         * Request the record audio permission.
         */
        fun requestRecordAudioPermission(activity: Activity) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.RECORD_AUDIO)) {
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.RECORD_AUDIO), 1)
            } else {
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.RECORD_AUDIO), 1)
            }
        }

        /**
         * Request the vibration permission.
         */
        fun requestVibrationPermission(activity: Activity) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.VIBRATE)) {
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.VIBRATE), 2)
            } else {
                ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.VIBRATE), 2)
            }
        }
    }
}