package com.pabelpm.myutils

/**
 * Created by Pabel on 26/03/2018.
 **/
class MyTimeUtils {
    companion object Const {
        var KEYLOG = MyTimeUtils::class.java.simpleName

        fun getTimeStampString(): String {
            val tsLong = System.currentTimeMillis() / 1000
            val ts = tsLong.toString()
            return ts
        }
    }
}