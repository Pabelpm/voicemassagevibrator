package com.pabelpm.myutils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.util.DisplayMetrics
import android.view.View

/**
 * Created by Pabel on 26/03/2018.
 **/
class UIUtils {
    companion object Const {
        var KEYLOG = UIUtils::class.java.simpleName

        //************************PROPORTION INTERFACE*******************************//

        fun dpToPx(ctx: Context, dp: Int): Int {
            val displayMetrics = ctx.resources.displayMetrics
            return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
        }

        fun pxToDp(ctx: Context, px: Int): Int {
            val displayMetrics = ctx.resources.displayMetrics
            return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
        }

        fun getBitmapFromView(view: View): Bitmap {
            val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
            val c = Canvas(bitmap)
            view.layout(view.left, view.top, view.right, view.bottom)
            view.draw(c)
            return bitmap
        }
    }
}