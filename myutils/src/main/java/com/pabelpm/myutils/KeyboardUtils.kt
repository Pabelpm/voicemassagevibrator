package com.pabelpm.myutils

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

/**
 * Created by Pabel on 26/03/2018.
 **/
class KeyboardUtils(act: Activity) {
    companion object Const {
        var KEYLOG = KeyboardUtils::class.java.simpleName

        //************************KEYBOARD*******************************//

        fun hideKeyboard(rootview: View) {
            val inputManager = rootview.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(rootview.windowToken, 0)
        }

        fun showKeyboard(rootview: View, editText: EditText) {
            val inputMethodManager = rootview.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.toggleSoftInputFromWindow(
                    editText.applicationWindowToken,
                    InputMethodManager.SHOW_FORCED, 0)
        }

        fun hideSoftKeyboard(activity: Activity) {
            if (activity.currentFocus != null) {
                val inputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
            }
        }
    }
}