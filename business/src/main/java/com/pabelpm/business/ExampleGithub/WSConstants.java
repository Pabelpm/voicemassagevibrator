package com.pabelpm.business.ExampleGithub;

/**
 * Created by Pabel on 27/03/2018.
 **/

public class WSConstants {

    public class Timeouts {
        public static final int CONNECTION_TIMEOUT = 120;
        public static final int READ_TIMEOUT = 120;
        public static final int WRITE_TIMEOUT = 120;
    }
}