package com.pabelpm.business.ExampleGithub;

import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Pabel on 27/03/2018.
 **/

public class WsConnectionGithub {

    private static OkHttpClient okHttpBuild() {
        OkHttpClient client;
        client = new OkHttpClient.Builder()
                .connectTimeout(WSConstants.Timeouts.CONNECTION_TIMEOUT, TimeUnit.SECONDS) // TIMEOUT conexion
                .readTimeout(WSConstants.Timeouts.READ_TIMEOUT, TimeUnit.SECONDS) // TIMEOUT read
                .writeTimeout(WSConstants.Timeouts.WRITE_TIMEOUT, TimeUnit.SECONDS) // TIMEOUT write
                .build();
        return client;
    }

    private static Gson gsonBuild() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .excludeFieldsWithoutExposeAnnotation()
                .registerTypeAdapter(String.class, new JsonDeserializer<String>() {
                    @Override
                    public String deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                        return json.getAsString() == null ? null : json.getAsString().trim();
                    }
                })
                .create();
        return gson;
    }

    private static Retrofit retrofitWithURL(String url) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url) // EndPoint
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gsonBuild())) // Tipo de conversion (JSON)
                .client(okHttpBuild()) // Cliente http a usar - usamos okhttp
                .build();

        return retrofit;
    }

    public static GithubApi getGithubApi() {
        return retrofitWithURL(GithubApi.API_URL)
                .create(GithubApi.class);
    }

}
