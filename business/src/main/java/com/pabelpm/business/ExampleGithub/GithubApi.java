package com.pabelpm.business.ExampleGithub;

import com.pabelpm.business.ExampleGithub.DataResponse.UserDetail;
import com.pabelpm.business.ExampleGithub.DataResponse.UserGithub;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Pabel on 27/03/2018.
 **/

public interface GithubApi {

    String API_URL = "https://api.github.com";
    String PATH_LIST = "/users?";
    String PATH = "users/{username}";

    //Requests
    @GET(PATH_LIST)
    Observable<List<UserGithub>> getUsersGithub(@Query("since") String since);

    @GET(PATH)
    Observable<UserDetail> getUserGithub(@Path("username") String username);
}
