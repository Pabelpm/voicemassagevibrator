package com.pabelpm.voicemassagevibrator

import android.support.v7.app.AppCompatActivity

import android.content.Intent
import com.pabelpm.voicemassagevibrator.main.MainActivity


class SplashActivity : AppCompatActivity() {

    override fun onResume() {
        super.onResume()
        startMainActivity()

//        val timer = Timer()
//        timer.schedule(timerTask {
//        }, 1000)

    }

    private fun startMainActivity(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
