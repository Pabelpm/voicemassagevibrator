package com.pabelpm.voicemassagevibrator.main

import android.os.Vibrator

/**
 * Created by Pabel on 04/04/2018.
 **/
interface MainInteractorInterface {

    fun getMyVibrations()

    fun isVibration():Boolean

    fun openSpotify(activity: MainActivity)
    fun openVoiceAccessSettings(activity: MainActivity)
    fun saveItemVibrator(customParamVibration: Long, customParamSleep: Long)
    fun vibrationPause(vibrator: Vibrator)
    fun vibratePattern(vibrator: Vibrator, vibration: Long, sleep: Long)
    fun vibratePattern(vibrator: Vibrator, progressSeekBar: Int)
    fun addProgressToVibrator(vibrator: Vibrator, progress: Int)
}