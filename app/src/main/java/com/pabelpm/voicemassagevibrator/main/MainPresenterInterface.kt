package com.pabelpm.voicemassagevibrator.main

import com.pabelpm.voicemassagevibrator.data.ItemVibrator
import io.realm.RealmResults

/**
 * Created by Pabel on 04/09/2018.
 **/
interface MainPresenterInterface {

    fun getMyVibrations()
    fun returnMyVibrations(realItemVibrators: RealmResults<ItemVibrator>?)
    fun getSongOfDay(mainActivity: MainActivity)

}