package com.pabelpm.voicemassagevibrator.main

import android.os.Build
import android.os.Handler
import android.support.annotation.RequiresApi
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.Snackbar
import android.util.Log
import android.widget.EditText
import android.widget.SeekBar
import com.airbnb.lottie.LottieAnimationView
import com.jakewharton.rxbinding2.view.RxView
import com.pabelpm.myutils.Constants
import com.pabelpm.myutils.Foreground
import com.pabelpm.voicemassagevibrator.adapter.VibratorAdapter
import com.pabelpm.voicemassagevibrator.data.ItemVibrator
import com.pabelpm.voicemassagevibrator.data.Repository
import io.realm.Realm
import io.realm.RealmChangeListener
import io.realm.RealmResults

@RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
/**
 * Created by Pabel on 28/03/2018.
 **/
class MainPresenter(var mainActivityInterface: MainActivityInterface) :MainPresenterInterface, SeekBar.OnSeekBarChangeListener, RealmChangeListener<Realm> {
    private val tag = Constants.APP_FILTER + MainPresenter::class.java.simpleName

    var mainInteractorInterface: MainInteractorInterface = MainInteractor(this@MainPresenter)

    private var onItemClickListenerAdapter: VibratorAdapter.OnItemClickListenerAdapter = object : VibratorAdapter.OnItemClickListenerAdapter {
        override fun onClickRow(vibration: Long, sleep: Long) {
            mainActivityInterface.setProgressSeekbar(0)
            mainInteractorInterface.vibratePattern(mainActivityInterface.getVibrator(), vibration, sleep)
        }

        override fun onClickDelete(item: ItemVibrator) {
            Repository.removeItemVibrator(item)
        }
    }

    init {
        Log.d(tag, "Init the MainPresenter")
        initializeInterface()
        initializeOnclicks()
        initializeListeners()
    }

    private fun initializeInterface() {

    }

    fun initializeOnclicks() {
        RxView.clicks(mainActivityInterface.getSlowButton()).subscribe(
                {
                    Log.d(tag, "Se ha recibido el clic SlowButton")
                    doActionOfButton(25)
                })

        RxView.clicks(mainActivityInterface.getMediumButton()).subscribe(
                {
                    Log.d(tag, "Se ha recibido el clic MediumButton")
                    doActionOfButton(50)
                })

        RxView.clicks(mainActivityInterface.getFastButton()).subscribe(
                {
                    Log.d(tag, "Se ha recibido el clic FastButton")
                    doActionOfButton(75)
                })

        RxView.clicks(mainActivityInterface.getFabMore()).subscribe(
                {
                    Log.d(tag, "Se ha recibido el clic FabMore")
                    if (mainActivityInterface.getSeekbar().progress > 90) {
                        mainActivityInterface.getSeekbar().progress = 100
                    } else {
                        mainActivityInterface.getSeekbar().progress = mainActivityInterface.getSeekbar().progress + 10
                    }
                    mainInteractorInterface.vibratePattern(mainActivityInterface.getVibrator(),mainActivityInterface.getSeekbar().progress)

                    mainInteractorInterface.addProgressToVibrator(mainActivityInterface.getVibrator(),mainActivityInterface.getSeekbar().progress)
                })

        RxView.clicks(mainActivityInterface.getFabLess()).subscribe(
                {
                    Log.d(tag, "Se ha recibido el clic FabLess")
                    if (mainActivityInterface.getSeekbar().progress < 10) {
                        mainActivityInterface.getSeekbar().progress = 0
                    } else {
                        mainActivityInterface.getSeekbar().progress = mainActivityInterface.getSeekbar().progress - 10
                    }
                    mainInteractorInterface.vibratePattern(mainActivityInterface.getVibrator(),mainActivityInterface.getSeekbar().progress)

                    mainInteractorInterface.addProgressToVibrator(mainActivityInterface.getVibrator(),mainActivityInterface.getSeekbar().progress)

                })


        RxView.clicks(mainActivityInterface.getAddFab()).subscribe(
                {
                    mainActivityInterface.cleanParams(mainActivityInterface.getEdCustomVibration() as EditText,mainActivityInterface.getEdCustomSleep() as EditText)
                    mainActivityInterface.showKeyboard()
                    Handler().postDelayed({
                        mainActivityInterface.getBottomSheetBehavior().state = (BottomSheetBehavior.STATE_EXPANDED)
                        (mainActivityInterface.getEdCustomVibration() as EditText).requestFocus()
                    }, 300)
                })

        RxView.clicks(mainActivityInterface.getAddDoneFab()).subscribe(
                {
                    Log.d(tag, "Se ha recibido el clic AddDoneFab")
                    if (checkIfEditextsAreNotEmpty()) {
                        if (mainActivityInterface.getBottomSheetBehavior().state != BottomSheetBehavior.STATE_EXPANDED) {
                            mainActivityInterface.getBottomSheetBehavior().state = (BottomSheetBehavior.STATE_EXPANDED)
                        } else {
                            mainActivityInterface.getBottomSheetBehavior().state = (BottomSheetBehavior.STATE_HIDDEN)
                        }
                        mainInteractorInterface.saveItemVibrator(mainActivityInterface.getCustomParamVibration(), mainActivityInterface.getCustomParamSleep())
                        Handler().postDelayed({
                            mainActivityInterface.hideKeyboard()
                        }, 400)
                    } else {
                        Snackbar.make(mainActivityInterface.getAddFab(), "Rellene todos los datos antes de guardar", Snackbar.LENGTH_SHORT).show()
                    }
                })

        RxView.clicks(mainActivityInterface.getBottomSheetBehaviorView()).subscribe(
                {
                    Log.d(tag, "Se ha recibido el clic BottomSheetBehaviorView")
                    mainActivityInterface.getBottomSheetBehavior().state = (BottomSheetBehavior.STATE_HIDDEN)
                    Handler().postDelayed({
                        mainActivityInterface.hideKeyboard()
                    }, 400)
                })

        RxView.clicks(mainActivityInterface.getPlayFab()).subscribe(
                {
                    Log.d(tag, "Se ha recibido el clic PlayButton")
                    if (checkIfEditextsAreNotEmpty()) {
                        mainInteractorInterface.vibratePattern(mainActivityInterface.getVibrator(),mainActivityInterface.getCustomParamVibration(), mainActivityInterface.getCustomParamSleep())
                        mainActivityInterface.setProgressSeekbar(0)
                    } else {
                        mainInteractorInterface.vibratePattern(mainActivityInterface.getVibrator(),mainActivityInterface.getProgressSeekBar())
                    }
                })

        RxView.clicks(mainActivityInterface.getPauseFab()).subscribe(
                {
                    Log.d(tag, "Se ha recibido el clic PauseButton")
                    mainInteractorInterface.vibrationPause(mainActivityInterface.getVibrator())
                })


        mainActivityInterface.getVibratorAdapter(onItemClickListenerAdapter).setInterface(onItemClickListenerAdapter)

    }

    private fun initializeListeners() {
        mainActivityInterface.getSeekbar().setOnSeekBarChangeListener(this)
        Realm.getDefaultInstance().addChangeListener(this)
    }

    /*********************Implementations Listeners*********************/
    //region Implementation Listeners
    override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
        //Log.d(tag, "Se ha recibido onProgressChanged con los argumentos progreso: $p1 y $p0 y $p2")
        mainActivityInterface.setProgressSeekbar(p1)
    }

    override fun onStartTrackingTouch(p0: SeekBar?) {
        Log.d(tag, "Se ha recibido onStartTrackingTouch")
    }

    override fun onStopTrackingTouch(p0: SeekBar?) {
        Log.d(tag, "Se ha recibido onStopTrackingTouch")
        mainInteractorInterface.vibratePattern(mainActivityInterface.getVibrator(),mainActivityInterface.getProgressSeekBar())
        mainActivityInterface.cleanParams(mainActivityInterface.getEdCustomVibration() as EditText,mainActivityInterface.getEdCustomSleep() as EditText)
    }


    override fun onChange(realm: Realm) {
        Log.i(tag, "Realm Database Change: $realm")
        realm.removeAllChangeListeners()
        mainActivityInterface.getVibratorAdapter(onItemClickListenerAdapter).update()
        Realm.getDefaultInstance().addChangeListener(this)
    }
    //endregion

    /*********************Auxiliar*********************/
    //region Auxiliar
    fun doActionOfButton(progress: Int) {
        mainActivityInterface.setProgressSeekbar(progress)
        mainInteractorInterface.vibratePattern(mainActivityInterface.getVibrator(),progress)
        mainActivityInterface.cleanParams(mainActivityInterface.getEdCustomVibration() as EditText,mainActivityInterface.getEdCustomSleep() as EditText)
    }

    fun checkIfEditextsAreNotEmpty(): Boolean {
        return !mainActivityInterface.getCustomParamSleep().equals(0L) && !mainActivityInterface.getCustomParamVibration().equals(0L)
    }

    override fun getMyVibrations() {
        mainInteractorInterface.getMyVibrations()
    }
    override fun returnMyVibrations(realItemVibrators: RealmResults<ItemVibrator>?) {
        mainActivityInterface.returnMyVibrations(realItemVibrators)
    }

    override fun getSongOfDay(mainActivity: MainActivity) {
        mainInteractorInterface.openSpotify(mainActivity)
    }


    /**
     * Listener to notify when app change to Foreground o Background state
     */
    internal var foregroundListener: Foreground.Listener = object : Foreground.Listener {
        override fun onBecameForeground() {
            Log.v(tag, "onBecameForeground")
        }

        override fun onBecameBackground() {
            Log.v(tag, "onBecameBackground")
            mainInteractorInterface.vibrationPause(mainActivityInterface.getVibrator())
        }
    }
    //endregion
}