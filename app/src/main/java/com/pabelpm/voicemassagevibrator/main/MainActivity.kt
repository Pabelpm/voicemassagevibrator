package com.pabelpm.voicemassagevibrator.main

import android.content.Context
import android.os.Bundle
import android.os.Vibrator
import android.support.design.widget.BottomSheetBehavior
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.TextView.OnEditorActionListener
import com.jakewharton.rxbinding2.view.RxView
import com.pabelpm.myutils.Constants
import com.pabelpm.myutils.KeyboardListener
import com.pabelpm.myutils.KeyboardUtils
import com.pabelpm.voicemassagevibrator.R
import com.pabelpm.voicemassagevibrator.adapter.VibratorAdapter
import com.pabelpm.voicemassagevibrator.data.ItemVibrator
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_main_bottom_sheet.*
import kotlinx.android.synthetic.main.activity_main_content.*


class MainActivity : AppCompatActivity(), MainActivityInterface {

    override fun getVibrator(): Vibrator {
        return this.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
    }

    private val tag = Constants.APP_FILTER + MainActivity::class.java.simpleName
    private lateinit var mainPresenterInterface: MainPresenterInterface

    init {
    }

    /*******************LifeCycle*********************/
    //region LifeCycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initRealm()

        mainPresenterInterface = MainPresenter(this@MainActivity)

        initListeners()
        initOnClickListener()

        mainPresenterInterface.getMyVibrations()
    }

    override fun onStop() {
        super.onStop()
        KeyboardUtils.hideSoftKeyboard(this@MainActivity)
    }

    override fun onDestroy() {
        super.onDestroy()
        val realm: Realm = Realm.getDefaultInstance()
        realm.close()
    }

    //endregion

    /*******************Interface Methods*********************/
    //region Interface Methods

    override fun returnMyVibrations(realItemVibrators: RealmResults<ItemVibrator>?) {
        rv_vibrations.adapter.notifyDataSetChanged()
    }

    //buttons
    override fun getSlowButton(): View {
        Log.d(tag, "getSlowButton")
        return bt_slow
    }

    override fun getMediumButton(): View {
        Log.d(tag, "getMediumButton")
        return bt_medium
    }

    override fun getFastButton(): View {
        Log.d(tag, "getFastButton")
        return bt_fast
    }

    override fun getFabLess(): View {
        Log.d(tag, "getFabLess")
        return fab_less
    }

    override fun getSeekbar(): SeekBar {
        Log.d(tag, "getSeekbar")
        return seekBar
    }

    override fun getFabMore(): View {
        Log.d(tag, "getFabMore")
        return fab_more
    }

    override fun getPlayFab(): View {
        Log.d(tag, "getPlayFab")
        return fab_play
    }

    override fun getPauseFab(): View {
        Log.d(tag, "getPauseFab")
        return fab_stop
    }

    override fun getAddFab(): View {
        Log.d(tag, "getAddFab")
        return fab_add
    }

    override fun getAddDoneFab(): View {
        Log.d(tag, "getAddDoneFab")
        return fab_add_done
    }

    override fun getBottomSheetBehavior(): BottomSheetBehavior<LinearLayout> {
        Log.d(tag, "getBottomSheetBehavior")
        return BottomSheetBehavior.from(bottom_sheet)
    }

    override fun getBottomSheetBehaviorView(): View {
        Log.d(tag, "getBottomSheetBehaviorView")
        return bottom_sheet
    }

    override fun getEdCustomVibration(): View {
        Log.d(tag, "getEdCustomVibration")
        return ed_custom_vibration
    }

    override fun getEdCustomSleep(): View {
        Log.d(tag, "getEdCustomSleep")
        return ed_custom_sleep
    }


    override fun getProgressSeekBar(): Int {
        return seekBar.progress
    }

    override fun setProgressSeekbar(progress: Int) {
        Log.d(tag, "setProgressSeekbar con el siguiente progreso: $progress")
        seekBar.progress = progress
        printProgressSeekBar(progress)
    }

    override fun getCustomParamSleep(): Long {
        return if (!ed_custom_sleep.text.toString().isEmpty())
            ed_custom_sleep.text.toString().toLong()
        else
            0L
    }

    override fun getCustomParamVibration(): Long {
        return if (!ed_custom_vibration.text.toString().isEmpty())
            ed_custom_vibration.text.toString().toLong()
        else
            0L
    }

    override fun getVibratorAdapter(onItemClickListenerAdapter: VibratorAdapter.OnItemClickListenerAdapter): VibratorAdapter {
        if (rv_vibrations.adapter == null) {
            rv_vibrations.layoutManager = LinearLayoutManager(this@MainActivity)
            rv_vibrations.adapter = VibratorAdapter(ArrayList(),onItemClickListenerAdapter)
            rv_vibrations.adapter = rv_vibrations.adapter as VibratorAdapter
        }
        return rv_vibrations.adapter as VibratorAdapter
    }

    override fun cleanParams(ed_custom_vibration: EditText, ed_custom_sleep: EditText) {
        ed_custom_vibration.setText("")
        ed_custom_sleep.setText("")
        ed_custom_sleep.clearFocus()

    }

    override fun printProgressSeekBar(progress: Int) {
        Log.d(tag, "printProgressSeekBar: $progress")
        tv_progress_seekbar.text = progress.toString()
    }


    override fun showKeyboard() {
        KeyboardUtils.showKeyboard(this.window.currentFocus, ed_custom_vibration as EditText)
    }
    override fun hideKeyboard() {
        KeyboardUtils.hideSoftKeyboard(this)
    }


    //endregion


    private fun initOnClickListener() {
        RxView.clicks(iv_cancion_dia).subscribe(
                {
                    Log.d(tag, "Se ha recibido el clic Song of day")
                    mainPresenterInterface.getSongOfDay(this@MainActivity)
                })
    }


    /*******************Auxiliar*********************/
    //region Auxiliar
    private fun initRealm() {
        Realm.init(this)
        val realmConfig = RealmConfiguration.Builder()
                .schemaVersion(0)
                .build()
        Realm.setDefaultConfiguration(realmConfig)
    }

    private fun initListeners() {
        KeyboardListener.addKeyboardToggleListener(this) { isVisible ->
            if (!isVisible) {
                if (getBottomSheetBehavior().state == BottomSheetBehavior.STATE_EXPANDED) {
                    getBottomSheetBehavior().state = (BottomSheetBehavior.STATE_HIDDEN)
                }
            }
        }

        ed_custom_vibration.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0.toString().length == 3)
                    ed_custom_sleep.requestFocus()
            }
        })

        ed_custom_sleep.setOnEditorActionListener(OnEditorActionListener { view, actionId, event ->
            val result = actionId and EditorInfo.IME_MASK_ACTION
            when (result) {
                EditorInfo.IME_ACTION_DONE -> {
                    fab_add_done.performClick()
                }
                else -> {
                    false
                }
            }
        })
    }
    //endregion
}
