package com.pabelpm.voicemassagevibrator.main

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import com.pabelpm.myutils.Constants
import com.pabelpm.myutils.MyTimeUtils
import com.pabelpm.voicemassagevibrator.data.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


/**
 * Created by Pabel on 04/04/2018.
 **/
class MainInteractor(var mainPresenterInterface: MainPresenterInterface) : MainInteractorInterface {
    override fun addProgressToVibrator(vibrator: Vibrator,progress: Int) {
vibratePattern(vibrator,progress)    }

    private val TAG = Constants.APP_FILTER + MainInteractor::class.java.simpleName

    init {
        Log.d(TAG, "Init the MainInteractor")
    }

    /*********************Implementations Vibration*********************/
    //region Implementation Vibration
    override fun getMyVibrations() {
        Repository.getAllVibrations()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ realItemVibrators ->
                    mainPresenterInterface.returnMyVibrations(realItemVibrators)
                })
    }


    override fun vibratePattern(vibrator: Vibrator,progressSeekBar: Int) {
        val vibration: Long = (progressSeekBar * 10).toLong()
        val sleep: Long = 1000 - vibration

        Log.d(TAG, "vibratePattern con vibration de: $vibration")
        Log.d(TAG, "vibratePattern con sleep de: $sleep")

        vibratePattern(vibrator,vibration,sleep)
    }

    override fun vibratePattern(vibrator: Vibrator,vibration: Long, sleep: Long) {
        Log.d(TAG, "vibratePattern con vibration de: $vibration")
        Log.d(TAG, "vibratePattern con sleep de: $sleep")

        val pattern = longArrayOf(sleep, vibration)
        vibrate(vibrator,pattern)
    }

    override fun vibrationPause(vibrator: Vibrator) {
        Log.d(TAG, "vibrationPause")

        vibrator.cancel()
    }

    override fun isVibration(): Boolean {
        Log.d(TAG, "isVibration ")
        return true
    }

    override fun openSpotify(activity: MainActivity) {
        try {
            //val uri = "spotify:album:4owrox6fGHGbH3GfyjdfTo"
            //val uri = "spotify:artist:5gOJTI4TusSENizxhcG7jB"
            val uri = "spotify:track:6qSbR4kxrvrSLg9emObfbX"
            val launcher = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            activity.startActivity(launcher)
        } catch (anfe: android.content.ActivityNotFoundException) {
            activity.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.spotify.music")))
        }
    }

    override fun openVoiceAccessSettings(activity: MainActivity) {
        var intent = activity.packageManager.getLaunchIntentForPackage("com.google.android.apps.accessibility.voiceaccess")
        if (intent == null) {
            // Bring user to the market or let them choose an app?
            intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("market://details?id="+ "com.google.android.apps.accessibility.voiceaccess")
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        activity.startActivity(intent)
    }

    private fun vibrate(vibrator: Vibrator,pattern: LongArray) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val effect = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                VibrationEffect.createWaveform(pattern, 0)
            } else {
                return
            }
            vibrator.vibrate(effect)
        } else {
            vibrator.vibrate(pattern, 0)
        }
    }

    override fun saveItemVibrator(customParamVibration: Long, customParamSleep: Long) {
        Repository.saveItemVibrator(MyTimeUtils.getTimeStampString(),customParamVibration,customParamSleep )

    }
}