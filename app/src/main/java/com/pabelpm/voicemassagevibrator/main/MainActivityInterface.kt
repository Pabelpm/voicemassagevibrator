package com.pabelpm.voicemassagevibrator.main

import android.os.Vibrator
import android.support.design.widget.BottomSheetBehavior
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.SeekBar
import com.pabelpm.voicemassagevibrator.adapter.VibratorAdapter
import com.pabelpm.voicemassagevibrator.data.ItemVibrator
import io.realm.RealmResults

/**
 * Created by Pabel on 28/03/2018.
 **/
interface MainActivityInterface {

    fun getSlowButton(): View
    fun getMediumButton(): View
    fun getFastButton(): View
    fun getFabLess(): View
    fun getSeekbar(): SeekBar
    fun getFabMore(): View
    fun getPlayFab(): View
    fun getPauseFab(): View
    fun getAddFab(): View
    fun getAddDoneFab(): View
    fun getBottomSheetBehavior(): BottomSheetBehavior<LinearLayout>
    fun getBottomSheetBehaviorView(): View
    fun getEdCustomVibration(): View
    fun getEdCustomSleep(): View

    fun getProgressSeekBar(): Int
    fun setProgressSeekbar(progress: Int)

    fun getCustomParamSleep(): Long
    fun getCustomParamVibration(): Long

    fun cleanParams(ed_custom_vibration: EditText,ed_custom_sleep: EditText)
    fun printProgressSeekBar(progress: Int)

    fun returnMyVibrations(realItemVibrators: RealmResults<ItemVibrator>?)
    fun showKeyboard()
    fun hideKeyboard()

    fun getVibrator():Vibrator
    fun getVibratorAdapter(onItemClickListenerAdapter: VibratorAdapter.OnItemClickListenerAdapter): VibratorAdapter
}