package com.pabelpm.voicemassagevibrator.data

import android.util.Log
import com.pabelpm.myutils.Constants
import io.reactivex.Observable
import io.realm.Realm
import io.realm.RealmResults


/**
 * Created by Pabel on 04/04/2018.
 **/
object Repository {
    private val TAG = Constants.APP_FILTER + Repository::class.java.simpleName

    fun getAllVibrations(): Observable<RealmResults<ItemVibrator>> {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val realmResults: RealmResults<ItemVibrator> = realm.where(ItemVibrator::class.java).findAll()
        realm.commitTransaction()

        return Observable.create<RealmResults<ItemVibrator>> { emitter ->
            try {
                emitter.onNext(realmResults)
                emitter.onComplete()
            } catch (e: Exception) {
                emitter.onError(e)
            }
        }
    }

    fun saveItemVibrator(id: String, vibration: Long, sleep: Long) {
        val realm: Realm = Realm.getDefaultInstance()
        // All writes must be wrapped in a transaction to facilitate safe multi threading
        realm.beginTransaction()
        // Add a itemVibrator
        val realmResults = realm.createObject(ItemVibrator::class.java)
        realmResults.id = id
        realmResults.customVibrator = vibration
        realmResults.customSleep = sleep
        Log.i(TAG, "Save item: $vibration , $sleep")
        realm.commitTransaction()
    }

    fun removeItemVibrator(itemVibrator: ItemVibrator) {
        val realm: Realm = Realm.getDefaultInstance()
        realm.executeTransaction { realmDataBase ->
            val result = realmDataBase.where(ItemVibrator::class.java).equalTo("id", itemVibrator.id).findAll()
            result.deleteAllFromRealm()
        }
        Log.i(TAG, "Item removed: ")
    }
}