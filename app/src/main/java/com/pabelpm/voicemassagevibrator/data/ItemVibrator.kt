package com.pabelpm.voicemassagevibrator.data

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required


/**
 * Created by Pabel on 04/04/2018.
 **/
open class ItemVibrator(@Required
                        var id: String,
                        var customSleep: Long,
                        var customVibrator: Long) : RealmObject() {

    constructor() : this("",0L,0L)
}