package com.pabelpm.voicemassagevibrator.adapter

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.RxView
import com.pabelpm.myutils.Constants
import com.pabelpm.voicemassagevibrator.R
import com.pabelpm.voicemassagevibrator.data.ItemVibrator
import com.pabelpm.voicemassagevibrator.data.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.list_item.view.*

/**
 * Created by Pabel on 11/04/2018.
 **/

class VibratorAdapter(var mItems: ArrayList<ItemVibrator>, var onItemClickListenerAdapter:OnItemClickListenerAdapter ) : RecyclerView.Adapter<VibratorAdapter.ViewHolder>() {
    private val tag = Constants.APP_FILTER + VibratorAdapter::class.java.simpleName

    init {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.list_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mItems[position]

        holder.vibrator.text = item.customVibrator.toString()
        holder.sleep.text = item.customSleep.toString()

        RxView.clicks(holder.container).subscribe(
                {
                    onItemClickListenerAdapter.onClickRow(item.customVibrator, item.customSleep)
                    Log.d(tag, "Se ha recibido el clic en el container Row")
                })
        RxView.clicks(holder.delete).subscribe(
                {
                    onItemClickListenerAdapter.onClickDelete(item)
                })
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    fun update() {
        Log.i(tag, " Actualizamos el recicler por recibir un cambio en Realm DataBase")
        Repository.getAllVibrations()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ realItemVibrators ->
                    mItems = ArrayList(realItemVibrators)
                    notifyDataSetChanged()
                })
    }

    fun setInterface(onItemClickListenerAdapter: OnItemClickListenerAdapter) {
        this.onItemClickListenerAdapter = onItemClickListenerAdapter
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val container = view.container
        val delete = view.bt_delete
        val vibrator = view.tv_list_item_vibrator
        val sleep = view.tv_list_item_sleep
    }

    interface OnItemClickListenerAdapter {
        fun onClickRow(vibration: Long, sleep: Long)
        fun onClickDelete(item: ItemVibrator)
    }

}